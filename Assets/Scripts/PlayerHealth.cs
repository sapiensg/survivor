﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    public PlayerHealthBar healthBar;
    public GameObject deadMenu;

    Animator animasi;

    public int maxHealthBar;
    public int currentHealthBar;
	public int health;
	public int numOfHearts;

	public Image[] hearts;
	public Sprite fullHeart;
	public Sprite emptyHeart;

    //public string diedScene;
    bool l;

    // Update is called once per frame
    void Start()
    {     
        animasi = GetComponent<Animator>();

        healthBar.SetMaxHealthBar(maxHealthBar);

        l = MainMenu.load;

        PlayerHealthData h =  SaveSystemPlayerHealth.LoadPlayerHealth();
        int ht = h.health;
        int hb = h.healthBar;
        

        if (l == true)
        {
            if ((ht > 0) && (hb > 0)){
                health = ht;
                currentHealthBar = hb;
            }
        }
        
    }

    void Update()
    {
        UpdateHeart();
        healthBar.SetHealthBar(currentHealthBar);
    }

    public void reduceHealth(int d)
    {

        animasi.Play("Player_hurt");

        if (currentHealthBar <= d)
        {
            
            currentHealthBar = currentHealthBar + 100 - d;
            health -= 1;
            SaveSystemPlayerHealth.SavePlayerHealth(this);
            if(health <= 0)
            {
                PlayerDie();
            }
            Debug.Log("kesini" + currentHealthBar);

        }
        else
        {
            currentHealthBar -=d;
            SaveSystemPlayerHealth.SavePlayerHealth(this);
        }
    }

    public void PlayerDie()
    {
        Destroy(gameObject);
        deadMenu.SetActive(true);
    }

    

    public void UpdateHeart()
    {
        if (health > numOfHearts)
        {
            health = numOfHearts;
        }

        for (int i = 0; i < hearts.Length; i++)
        {
            if(i < health)
            {
                hearts[i].sprite = fullHeart;
            }
            else
            {
                hearts[i].sprite = emptyHeart;
            }

            if(i < numOfHearts)
            {
                hearts[i].enabled = true;
            }
            else
            {
                hearts[i].enabled = false;
            }
        }
    }
}
