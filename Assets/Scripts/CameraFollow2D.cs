﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow2D : MonoBehaviour
{
    
    public GameObject Player;

    public float timeOffset;
    
    public Vector2 posOffset;

    private Vector3 velocity;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //transform.position = new Vector3(Player.transform.position.x, Player.transform.position.y, -10);
        Vector3 startPos = transform.position;

        Vector3 endPos = Player.transform.position;
        endPos.x += posOffset.x;
        endPos.y+= posOffset.y;
        endPos.z = -10;

        transform.position = Vector3.SmoothDamp(startPos, endPos, ref velocity, timeOffset);
    }
}
