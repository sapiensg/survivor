﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerHealthData
{
	public int health;
	public int healthBar;

	public PlayerHealthData(PlayerHealth player)
	{
		health = player.health;
		healthBar = player.currentHealthBar;

	}
}
