﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
	public string newGameScene;

	public static bool load;

	public GameObject continueButton;


    // Start is called before the first frame update
    void Start()
    {
        PlayerHealthData h =  SaveSystemPlayerHealth.LoadPlayerHealth();
        int ht = h.health;

        if(ht <= 0)
        {
            continueButton.SetActive(false);
        }else
        {
            continueButton.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Continue()
    {
    	load = true;


    	SceneManager.LoadScene(newGameScene);

    }

    public void NewGame()
    {
    	load = false;
    	SceneManager.LoadScene(newGameScene);
    }

    public void Exit()
    {
    	Application.Quit();
    }
}
