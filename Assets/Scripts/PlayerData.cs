﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
	public float[] respawnP;

	public PlayerData(PlayerController player)
	{
		//health = player.health;
		respawnP = new float[3];
		respawnP[0] = player.transform.position.x;
		respawnP[1] = player.transform.position.y;
		respawnP[2] = player.transform.position.z;
	}

	// public PlayerHealthData(PlayerHealth player)
	// {
	// 	healt = player.health;
	// }
}
