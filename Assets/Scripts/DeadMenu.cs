﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class DeadMenu : MonoBehaviour
{

	public string mainMenu;
	public string currentScene;
    public GameObject ini;
    // Start is called before the first frame update
    void Start()
    {
    	ini.SetActive(false);
        
    }

    // Update is called once per frame
    // void Update()
    // {
        
    // }

    public void Restart()
    {
    	//Application.LoadLevel(Application.loadedLevel);
    	SceneManager.LoadScene(currentScene);
    }

    public void MainMenu()
    {
    	SceneManager.LoadScene(mainMenu);
    }
}
