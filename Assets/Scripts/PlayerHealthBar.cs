﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthBar : MonoBehaviour
{
	public Slider slider;

    public void SetMaxHealthBar(int health)
    {
        slider.maxValue = health;
        slider.value = health;
    }

    public void SetHealthBar(int health)
    {
        slider.value = health;
    }
}
