﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class Catatan : MonoBehaviour
{
    public string mainMenu;
    public GameObject trigggerFinish;
    public GameObject menuF;
    // Start is called before the first frame update
    void Start()
    {
    	menuF.SetActive(false);
    	//gameObject.SetActive(false);
    	trigggerFinish.SetActive(true);
        
    }

    public void MainMenu()
    {
    	SceneManager.LoadScene(mainMenu);
    }

    void OnTriggerEnter2D(Collider2D c)
    {
    	if(c.tag == "Player")
    	{
    		menuF.SetActive(true);
    		//gameObject.SetActive(true);
    	}
    }

    void OnTriggerExit2D(Collider2D c)
    {
    	if(c.tag == "Player")
    	{
    		menuF.SetActive(false);
    	}
    }
}
