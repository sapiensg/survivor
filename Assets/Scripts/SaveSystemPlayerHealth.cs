﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystemPlayerHealth
{
	public static void SavePlayerHealth(PlayerHealth player)
	{
   		BinaryFormatter formatter = new BinaryFormatter();
   		string path = Application.persistentDataPath + "/playerhealth.dat";
   		FileStream stream = new FileStream(path, FileMode.Create);

   		PlayerHealthData data = new PlayerHealthData( player);

   		formatter.Serialize(stream, data);
   		stream.Close();
   	}

	public static PlayerHealthData LoadPlayerHealth()
	{
		string path = Application.persistentDataPath + "/playerhealth.dat";
		if(File.Exists(path))
		{
			BinaryFormatter formatter = new BinaryFormatter();
			FileStream stream = new FileStream(path, FileMode.Open);

			PlayerHealthData data = formatter.Deserialize(stream) as PlayerHealthData;
			stream.Close();

			return data;
		}
		else
		{
			return null;
		}
	}

}
