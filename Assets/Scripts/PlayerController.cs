﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
	Animator animasi;
	Rigidbody2D rb2d;
	SpriteRenderer mySprite;
	public float kecepatan;
    public Vector3 respawnPoint;

    bool l;
	bool isGrounded;

	public Transform groundCheck;
    public Transform groundCheck1;
    public Transform groundCheck2;

    //attack
    bool attacking;
    public Transform attackPoint;
    public float attackRange = 0.5f;
    public LayerMask dinoLayer;
    public int attackDamage = 30;

    //dialog

    // Start is called before the first frame update
    void Start()
    {
    	animasi = GetComponent<Animator>();
    	rb2d = GetComponent<Rigidbody2D>();
    	mySprite = GetComponent<SpriteRenderer>();
        respawnPoint = transform.position;

        //MainMenu = gameObject.GetComponent<MainMenu>();
        l = MainMenu.load;
        PlayerHealthData h =  SaveSystemPlayerHealth.LoadPlayerHealth();
        int ht = h.health;
        

        if (l == true)
        {
            if (ht > 0){
                PlayerData data =  SaveSystem.LoadPlayer();
                Vector3 pos;
                pos.x = data.respawnP[0];
                pos.y = data.respawnP[1];
                pos.z = data.respawnP[2];
                transform.position = pos;
            }
        }

        Debug.Log(l);
    }

    private void FixedUpdate()
    {
		if (Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground")) ||
            Physics2D.Linecast(transform.position, groundCheck1.position, 1 << LayerMask.NameToLayer("Ground")) ||
            Physics2D.Linecast(transform.position, groundCheck2.position, 1 << LayerMask.NameToLayer("Ground")) ||
            Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Dino")) ||
            Physics2D.Linecast(transform.position, groundCheck1.position, 1 << LayerMask.NameToLayer("Dino")) ||
            Physics2D.Linecast(transform.position, groundCheck2.position, 1 << LayerMask.NameToLayer("Dino"))){
			isGrounded = true;
		}else{
			isGrounded = false;
		}

    	if(Input.GetKey("right")){
            if(isGrounded)
    		  rb2d.velocity = new Vector2(kecepatan, rb2d.velocity.y);
    		animasi.Play("Player_run");
            transform.localScale = new Vector3(1, 1, 1);
            
    	}
    	else if(Input.GetKey("left")){
            if(isGrounded)
    		  rb2d.velocity = new Vector2(-kecepatan, rb2d.velocity.y);
    		animasi.Play("Player_run");
            transform.localScale = new Vector3(-1, 1, 1);
    	}
    	else{
            if(isGrounded && !attacking)
                animasi.Play("player_idle");
			rb2d.velocity = new Vector2(0,rb2d.velocity.y);
    	}

		if(Input.GetKey("up") && isGrounded)
		{
			rb2d.velocity = new Vector2(rb2d.velocity.x, 40);
			animasi.Play("Player_jump");
		}

        if(Input.GetKeyDown(KeyCode.Space))
        {
            attacking = true;
            animasi.SetTrigger("Attack");

            Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, dinoLayer);

            foreach (Collider2D enemy in hitEnemies)
            {
                Debug.Log("we hit " + enemy.name);
                enemy.GetComponent<DinoScript>().takeDamage(attackDamage);
            }
            
        }
        
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.tag == "killFloor")
        {

            GetComponent<PlayerHealth>().reduceHealth(100);
            transform.position = respawnPoint;
        }

        if(c.tag == "checkPoint")
        {
            SaveSystem.SavePlayer(this);
            respawnPoint = c.transform.position;
        }
    }

    void OnDrawGizmosSelected()
    {
        if (attackPoint == null)
            return;
        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }

}


