﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DialogScript : MonoBehaviour
{
    public TextMeshProUGUI textDiplay;
    public string[] informasi;
    private int index;
    public float typingSpeed;

    public GameObject continueButton;

    public GameObject dialogUI;

    void Start()
    {
    	dialogUI.SetActive(false);
        continueButton.SetActive(false);
    }

    void Update()
    {
    	if(textDiplay.text == informasi[index])
    	{
    		continueButton.SetActive(true);
    	}else
        {
            continueButton.SetActive(false);
        }
    }

    // IEnumerator Type()
    // {
    // 	foreach(char huruf in informasi[index].ToCharArray())
    // 	{
    // 		textDiplay.text += huruf;
    // 		yield return new WaitForSeconds(typingSpeed);
    // 	}
    // }

    public void type()
    {
        textDiplay.text = informasi[index];
    }

    public void NextSentence()
    {
    	if(index < informasi.Length - 1)
    	{
    		index++;
    		textDiplay.text = "";
    		//StartCoroutine(Type());
            type();
    	}else
    	{
    		textDiplay.text = "";
    	}
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.tag == "Player")
        {
            textDiplay.text = "";
            index = 0;
        	//StartCoroutine(Type());
            type();
        	dialogUI.SetActive(true);
        }
    }

    void OnTriggerExit2D(Collider2D e)
    {
    	if (e.tag == "Player")
        {
        	//StartCoroutine(Type());
        	dialogUI.SetActive(false);
        	textDiplay.text = "";
        	index = 0;
        }
    }
}
