﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystemDino
{
    public static void SaveDino(DinoScript dino)
	{
   		BinaryFormatter formatter = new BinaryFormatter();
   		string path = Application.persistentDataPath + "/dino.dat";
   		FileStream stream = new FileStream(path, FileMode.Create);

   		DinoData data = new DinoData(dino);

   		formatter.Serialize(stream, data);
   		stream.Close();
   	}

	public static DinoData LoadDino()
	{
		string path = Application.persistentDataPath + "/dino.dat";
		if(File.Exists(path))
		{
			BinaryFormatter formatter = new BinaryFormatter();
			FileStream stream = new FileStream(path, FileMode.Open);

			DinoData data = formatter.Deserialize(stream) as DinoData;
			stream.Close();

			return data;
		}
		else
		{
			return null;
		}
	}
}
