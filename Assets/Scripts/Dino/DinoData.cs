﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DinoData
{
    public int[] healthD;

	public DinoData(DinoScript dino)
	{
		//string s = dino.gameObject.name;
		healthD = new int[4];
		if(dino.gameObject.name == "Carnotaurus"){
			healthD[0] = dino.currentHealth;
		}
		else if(dino.gameObject.name == "Diplodocus")
		{
			healthD[1] = dino.currentHealth;
		}
		else if(dino.gameObject.name == "Pterodactyl")
		{
			healthD[2] = dino.currentHealth;
		}
		else if (dino.gameObject.name == "Parasaurolopus")
		{
			healthD[3] = dino.currentHealth;
		}

	}
}
