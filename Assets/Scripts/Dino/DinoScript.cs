﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DinoScript : MonoBehaviour
{

	public Animator anim;
    Rigidbody2D rb2d;
    SpriteRenderer mySprite;

    public Text nilai;

    public string dinoWalk;
    public string dinoAttack;
    public string triggerDie;
    public string triggerHurt;


	public int maxHealth = 100;
	public int currentHealth;

    public float speed;
    public bool moveRight;

    public Transform attackPoint;
    public float attackRange = 0.5f;
    public LayerMask playerLayer;
    public int attackDamage;

    public bool attacking = false;
    bool l;
    public Color c;
    // Start is called before the first frame update
    void Start()
    {
        
        //anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        mySprite = GetComponent<SpriteRenderer>();
        currentHealth = maxHealth;

        // DinoData d =  SaveSystemDino.LoadDino();
        // int c = d.healthD[0];
        // int di = d.healthD[1];
        // int pt = d.healthD[2];
        // int pr = d.healthD[3];

        // PlayerHealthData h =  SaveSystemPlayerHealth.LoadPlayerHealth();
        // int ht = h.health;

        // l = MainMenu.load;

        // if(l && ht > 0)
        // {
        //     if(gameObject.name == "Carnotaurus")
        //     {
        //         currentHealth = c;
        //     }else if (gameObject.name == "Diplodocus")
        //     {
        //         currentHealth = di;
        //     } else if (gameObject.name == "Pterodactyl")
        //     {
        //         currentHealth = pt;
        //     }else if (gameObject.name == "Parasaurolopus")
        //     {
        //         currentHealth = pr;
        //     }
        // } else
        // {
        // }

        if(currentHealth <= 0){
            Die();
        }

        nilai.text = currentHealth.ToString();
    	
    }

    // Update is called once per frame
    void Update()
    {
        //SaveSystemDino.SaveDino(this);
        transform.Translate(Vector3.right*speed*Time.deltaTime);
        if(currentHealth > 0)
        {
            if (Physics2D.Linecast(transform.position, attackPoint.position, 1 << LayerMask.NameToLayer("Player"))){
                attacking = true;
            }else{
                attacking = false;
            }
        } else
        {
            Die();
        }

        if (attacking == true)
        {
            //attack();
            //anim.Play(dinoAttack);
        }

        
    }

    public void takeDamage(int damage)
    {
    	currentHealth -= damage;
        mySprite.color = c;
        Invoke("resetColor", 0.2f);

        if(currentHealth<0)
        {
            currentHealth = 0;
        }
        nilai.text = currentHealth.ToString();
        
    	anim.SetTrigger(triggerHurt);

    	if (currentHealth <= 0)
    	{
    		Die();
    	}
    }

    void resetColor()
    {
        mySprite.color = new Color(255,255,255);
    }

    void destroyObject()
    {
        Destroy(gameObject);
    }

    void Die()
    {
    	anim.SetBool(triggerDie, true);

    	GetComponent<Collider2D>().enabled = false;
    	GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionY;
        //GetComponent<Animator>().enabled = false;

        Invoke("destroyObject", 1.5f);
        

    	this.enabled = false;
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.gameObject.CompareTag("areaM"))
        {
            if(moveRight)
            {
                moveRight = false;
                transform.eulerAngles = new Vector3(0, 0, 0);
            }
            else
            {
                moveRight = true;
                transform.eulerAngles = new Vector3(0, -180, 0);
            }
        }

        if(c.gameObject.CompareTag("Player"))
        {
            attack();
        }
    }

    public void attack()
    {
            Collider2D[] hitPlayer = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, playerLayer);
            if (currentHealth > 0)
            {
                anim.Play(dinoAttack);
                foreach (Collider2D player in hitPlayer)
                {
                    player.GetComponent<PlayerHealth>().reduceHealth(attackDamage);
                }

            }
    }

    void OnDrawGizmosSelected()
    {
        if (attackPoint == null)
            return;
        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }
}
